import java.util.Collections;
import java.util.LinkedList;

public class Deck {
	
	private LinkedList<Card> cardList = new LinkedList<Card>();
	
	public Deck(int nbBox) {
		
		Value[] myValues = Value.values();
		Color[] myColors = Color.values();
		
		for(int i=0;i<nbBox;i++) {
			for(int j=0;j<Color.values().length;j++) {
				for(int k=0;k<Value.values().length;k++) {
					cardList.add(new Card(myValues[k], myColors[j]));
				}
			}
		}
		//Shuffle the deck once
		Collections.shuffle(cardList);
	}
	
	public Card draw() throws EmptyDeckException {
		if(cardList.size()==0) {
			throw new EmptyDeckException("Pas assez de cartes !");
		}
		Card nextCard = cardList.pollFirst();
		return nextCard;
	}
	
	@Override
	public String toString() {
		return cardList.toString();
	}

}
