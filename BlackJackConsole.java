import java.util.Scanner;

public class BlackJackConsole {
	
	public BlackJackConsole() {
		System.out.println("Welcome to the BlackJack table. Let's play !");
	}
	
	public static void main(String[] args) throws EmptyDeckException {
		new BlackJackConsole();
		Scanner scan = new Scanner(System.in);
		
		BlackJack blackJack = new BlackJack();
		System.out.println("The bank draw : "+blackJack.getBankHandString()+" : "+blackJack.getBankBest());
		System.out.println("You draw : "+blackJack.getPlayerHandString()+" : "+blackJack.getPlayerBest());
		
		
		while(blackJack.isGameFinished()!=true) {
			System.out.println("Do you want anohter card ? [y/n]");
			switch (scan.next()) {
			case "y":
				try {
				blackJack.playerDrawAnotherCard();
				System.out.println("Your new hand : "+blackJack.getPlayerHandString());
				}catch(EmptyDeckException ex){
					System.err.println(ex.getMessage());
					System.out.println("Error, the deck has insuffisent cards !");
					System.exit(-1);
				}
				break;

			case "n":
				try {
					blackJack.bankLastTurn();
					System.out.println("The bank draw cards. New hand : "+blackJack.getBankHandString());
				}catch(EmptyDeckException ex) {
					System.err.println(ex.getMessage());
					System.exit(-1);
				}
				break;
			}
		}
		System.out.println("Player best : "+blackJack.getPlayerBest());
		System.out.println("Bank best : "+blackJack.getBankBest());
		if(blackJack.isPlayerWinner() == blackJack.isBankWinner())
			System.out.println("Draw !");
		else if(blackJack.isBankWinner())
			System.out.println("Bank win !");
		else if(blackJack.isPlayerWinner())
			System.out.println("Player win !");
	}
}
