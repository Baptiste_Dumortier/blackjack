import java.util.LinkedList;
import java.util.List;

public class Hand {
	
	private LinkedList<Card> cardList = new LinkedList<Card>();
	
	public Hand() {
		
	}
	
	public void add(Card card) {
		cardList.add(card);
	}
	
	public void clear() {
		cardList.removeAll(cardList);
	}
	
	public List<Integer> count(){
		List<Integer> results = new LinkedList<Integer>();
		results.add(0);
		for(int i=0;i<cardList.size();i++) {
			int listLenght = results.size();
			for(int j=0;j<listLenght;j++) {
				int cardVal = cardList.get(i).getPoints();
				results.set(j, results.get(j)+cardVal);
				if(cardVal==1) {
					results.add(results.get(j)+10);
				}
			}
		}
		return results;
	}
	
	public int best() {
		List<Integer> count = count();
		for(int i=1;i<count.size();i++) {
			if(count.get(i)>21)
				return count.get(i-1);
		}
		return count.get(count.size()-1);
		
	}
	
	public List<Card> getCardList(){
		return cardList;
	}
	
	@Override
	public String toString() {
		return cardList.toString()+" : "+count();
	}
	
}
