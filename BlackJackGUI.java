import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

public class BlackJackGUI implements ActionListener{
	
	private BlackJack bj;
	private JPanel playerPanel;
	private JPanel bankPanel;
	private JButton anotherButton = new JButton("Another Card");
	private JButton noMoreButton = new JButton("No More Card");
	private JButton resetButton = new JButton("Reset");


	public BlackJackGUI() {
		
		bj = new BlackJack();
		anotherButton.setActionCommand("another");
		anotherButton.addActionListener(this);
		noMoreButton.setActionCommand("nomore");
		noMoreButton.addActionListener(this);
		resetButton.setActionCommand("reset");
		resetButton.addActionListener(this);
		
		
		JFrame frame = new JFrame("BlackJack GUI");
		frame.setMinimumSize(new Dimension(640, 480));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		frame.pack();
		frame.setVisible(true);
		
		JPanel topPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 30, 20));
		topPanel.add(anotherButton);
		topPanel.add(noMoreButton);
		topPanel.add(resetButton);
		
		frame.add(topPanel, BorderLayout.NORTH);
		
		FlowLayout flowCards = new FlowLayout(FlowLayout.LEFT, 10,10);
		JPanel centerPanel = new JPanel(new GridLayout(2, 2, 20, 20));
		bankPanel = new JPanel(flowCards);
		bankPanel.setBorder(BorderFactory.createTitledBorder(new EmptyBorder(20, 50, 20, 20), "Bank"));
		playerPanel = new JPanel(flowCards);
		playerPanel.setBorder(BorderFactory.createTitledBorder(new EmptyBorder(20, 50, 20, 20),"Player"));
		centerPanel.add(bankPanel);
		centerPanel.add(playerPanel);

		frame.add(centerPanel, BorderLayout.CENTER);
		
		try {
			updatePlayerPanel();
			updateBankPanel();
		}catch(FileNotFoundException ex) {
			ex.getMessage();
		}
		
	}
	
	//Main
	public static void main(String[] args) {
		new BlackJackGUI();
	}
	
	//Methods
	public void addToPanel(JPanel p, String token) throws FileNotFoundException {
		File file = new File("./img/card_"+token+".png");
		if (!file.exists()) {
			throw new FileNotFoundException("Can't find "+file.getPath());
		}
		ImageIcon icon = new ImageIcon(file.getPath()); // Create the image from the filename
		JLabel label = new JLabel(icon); // Associate the image to a label
		p.add(label); // Add the label to a panel
	}
	
	public void updatePlayerPanel() throws FileNotFoundException{
		playerPanel.removeAll();
		for(int i=0;i<bj.getPlayerCardList().size();i++) {
			Card curentCard = bj.getPlayerCardList().get(i);
			addToPanel(playerPanel, curentCard.getColorName()+"_"+curentCard.getValueSymbole());
		}
		playerPanel.add(new JLabel("Player best : "+bj.getPlayerBest()));
		playerPanel.updateUI();
	}
	
	public void updateBankPanel() throws FileNotFoundException{
		bankPanel.removeAll();
		for(int i=0;i<bj.getBankCardList().size();i++) {
			Card curentCard = bj.getBankCardList().get(i);
			addToPanel(bankPanel, curentCard.getColorName()+"_"+curentCard.getValueSymbole());
		}
		bankPanel.add(new JLabel("Bank best : "+bj.getBankBest()));
		bankPanel.updateUI();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
			switch(e.getActionCommand()) {
			case "another":
				try {
					bj.playerDrawAnotherCard();
				}catch (EmptyDeckException ex){
					JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					System.exit(-1);
				}
				break;
			case "nomore":
				try {
					bj.bankLastTurn();
				}catch (EmptyDeckException ex) {
					JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					System.exit(-1);
				}
				break;
			case "reset":
				try {
					bj.reset();
				} catch (EmptyDeckException ex) {
					JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					System.exit(-1);
				}
				break;
				default: System.out.println("oui");
			}
			try {
				updateBankPanel();
				updatePlayerPanel();
			} catch (FileNotFoundException ex) {
				System.err.println(ex.getMessage());
				System.exit(-1);
			}
		if(bj.isGameFinished()==true) {
			noMoreButton.setEnabled(false);
			anotherButton.setEnabled(false);
			if(bj.isPlayerWinner() == bj.isBankWinner())
				try {
					addToPanel(playerPanel, "draw");
					addToPanel(bankPanel, "draw");
				}catch (FileNotFoundException ex) {
					JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					System.exit(-1);
				}
			else if(bj.isBankWinner())
				try {
					addToPanel(bankPanel, "winner");
					addToPanel(playerPanel, "looser");
				}catch (FileNotFoundException ex) {
					JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					System.exit(-1);
				}
			else if(bj.isPlayerWinner())
				try {
					addToPanel(bankPanel, "looser");
					addToPanel(playerPanel, "winner");
				}catch (FileNotFoundException ex) {
					JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					System.exit(-1);
				}
		}else {
			noMoreButton.setEnabled(true);
			anotherButton.setEnabled(true);
		}
	}
	
}
