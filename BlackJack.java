import java.util.LinkedList;

public class BlackJack {
	
	//
	private Deck deck;
	private Hand playerHand;
	private Hand bankHand;
	private boolean gameFinished;
	
	//Constructor
	public BlackJack() {
		deck = new Deck(4);
		playerHand = new Hand();
		bankHand = new Hand();
		//Reset both hands
		try {
			reset();
		} catch (EmptyDeckException ex) {
			System.err.println(ex.getMessage());
			System.out.println("Error, the deck has insuffisent cards !");
			System.exit(-1);
		}
	}
	
	//Methods
	public void reset() throws EmptyDeckException {
		playerHand.clear();
		bankHand.clear();
		bankHand.add(deck.draw());
		playerHand.add(deck.draw());
		playerHand.add(deck.draw());
		gameFinished = false;
	}
	
	public String getPlayerHandString() {
		return playerHand.toString();
	}
	
	public String getBankHandString() {
		return bankHand.toString();
	}
	
	public int getPlayerBest() {
		return playerHand.best();
	}
	
	public int getBankBest() {
		return bankHand.best();
	}
	
	public boolean isPlayerWinner() {
		int playerBest = getPlayerBest();
		int bankBest = getBankBest();
		if(gameFinished==true)
			if(playerBest <= 21 && (playerBest > bankBest || bankBest > 21))
				return true;
		return false;
			
	}
	
	public boolean isBankWinner() {
		int playerBest = getPlayerBest();
		int bankBest = getBankBest();
		if(gameFinished==true)
			if(bankBest <= 21 && (bankBest > playerBest || playerBest > 21))
				return true;
		return false;
			
	}
	
	public boolean isGameFinished() {
		return gameFinished;
	}
	
	public void playerDrawAnotherCard() throws EmptyDeckException{
		if(isGameFinished()==false)
			playerHand.add(deck.draw());
			if(getPlayerBest() > 21)
				gameFinished = true;
	}
	
	public void bankLastTurn() throws EmptyDeckException{
		int playerBest = getPlayerBest();
		if(gameFinished==false && (playerBest<=21))
			while(playerBest>getBankBest())
				try {
					bankHand.add(deck.draw());
				}catch(EmptyDeckException ex){
					ex.getMessage();
				}
			gameFinished=true;
	}
	
	public LinkedList<Card> getPlayerCardList(){
		
		LinkedList<Card> copyList = new LinkedList<Card>(playerHand.getCardList());
		return copyList;
	}
	
	public LinkedList<Card> getBankCardList(){
		
		LinkedList<Card> copyList = new LinkedList<Card>(bankHand.getCardList());
		return copyList;
	}
	
}
