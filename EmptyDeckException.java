public class EmptyDeckException extends java.lang.Exception {

	public EmptyDeckException(String message){
		super(message);
	}
	
}